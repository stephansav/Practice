<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: guillaumeloulier
 * Date: 23/07/2018
 * Time: 11:14
 */

namespace App\UI\Action;

use App\UI\Responder\Interfaces\HomeResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeAction.
 *
 * @Route(
 *     path="/",
 *     name="home"
 * )
 */
final class HomeAction
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(
        Request $request,
        HomeResponderInterface $responder
    ): Response {

        return $responder($request);
    }
}
